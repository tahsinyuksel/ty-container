<?php

namespace TyContainer;

use TyContainer\ContainerInterface;

abstract class ContainerAbstract implements ContainerInterface
{
	protected static $map;

	protected static $called = [];

	protected static $parameters = [];

	public function __construct($map, $parameters = [])
	{
		self::$map = $map;
		self::$parameters = $parameters;
	}

	public function get($name, $force = false)
	{
		if($this->has($name) && false == $force) {
			return $this->getCalled($name);
		}

		foreach (self::$map as $value) {
			if($value['name'] == $name) {

				$arguments = [];

				foreach ($value['arguments'] as $argument) {
					
					if(is_string($argument) && substr($argument, 0, 2) == '@@') {						
						$arguments[] = $this->get(ltrim($argument, '@@'));
						continue;
					} else if(is_string($argument) && substr($argument, 0, 2) == '++') {
						$arguments[] = $this->getParameter(ltrim($argument, '++'));
						continue;
					}

					$arguments[] = $argument;					
				}

				return $this->call($name, $value['class'], $arguments);
			}
		}

		return null;
	}

	private function call($name, $class = '', $arguments) 
	{
		$reflect  = new \ReflectionClass($class);
		$obj = $reflect->newInstanceArgs($arguments);

		$this->set($name, $obj);

		return $obj;
	}

	private function getCalled($name)
	{
		return $this->has($name) ? self::$called[$name] : null;
	}

	public function has($name)
	{
		return isset(self::$called[$name]);
	}

	public function set($name, $object)
	{
		self::$called[$name] = $object;
	}

	public function getParameter($name)
	{
		return isset(self::$parameters[$name]) ? self::$parameters[$name] : null;
	}

	public function setParameter($name, $value)
	{
		self::$parameters[$name] = $value;
	}

	public function hasParameter($name)
	{
		return isset(self::$parameters[$name]);
	}
}