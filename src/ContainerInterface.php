<?php

namespace TyContainer;

Interface ContainerInterface
{
	/**
     * Sets a service.
     *
     * @param string $id      	The service identifier
     * @param object $object    The service object
     */
    public function set($id, $object);

    /**
     * Gets a service.
     *
     * @param string $id              	The service identifier
     * @param bool    $force 			Is new instance
     *
     * @return object The associated service
     *
     * @throws Exception
     */
    public function get($id, $force = false);

    /**
     * Returns true if the given service is defined.
     *
     * @param string $id The service identifier
     *
     * @return bool true if the service is defined, false otherwise
     */
    public function has($id);

    /**
     * Gets a parameter.
     *
     * @param string $name The parameter name
     *
     * @return mixed The parameter value
     *
     * @throws Exception if the parameter is not defined
     */
    public function getParameter($name);

    /**
     * Checks if a parameter exists.
     *
     * @param string $name The parameter name
     *
     * @return bool The presence of parameter in container
     */
    public function hasParameter($name);

    /**
     * Sets a parameter.
     *
     * @param string $name  The parameter name
     * @param mixed  $value The parameter value
     */
    public function setParameter($name, $value);
}