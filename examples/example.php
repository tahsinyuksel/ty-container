<?php

// require composer autoload
require_once __DIR__ . '/../vendor/autoload.php';

use TyContainer\Container;

echo 'examples';

class Manager {

	protected $adapter = null;
	protected $db = null;

	public function __construct($adapter = null, $db = null)
	{
		$this->adapter = $adapter;		
		$this->db = $db;	
	}

	public function write()
	{
		if(is_object($this->adapter)) {
			$this->adapter->write();
		}

	}

	public function connect()
	{
		print_r('db parameters get -->');
		print_r($this->db);
	}
}

class FileAdapter {
	
	protected $service = null;

	public function __construct($service = null, $name = '')
	{
		$this->service = $service;
		print_r(sprintf('(file name: %s)', $name));
	}

	public function write()
	{
		print_r('file write.');
	}
}

class ConsoleAdapter {
	
	protected $service = null;

	public function __construct($service = null)
	{
		$this->service = $service;
	}

	public function write()
	{
		print_r('console write.');
	}
}


print_r('<br><b>0: Container definations</b><br>');
/**
* Container map definations
*/
$map = [
	['name'=> 'file_adapter', 'class'=> 'FileAdapter', 'arguments'=> [null, 'file_2019.csv'] ],
	['name'=> 'manager', 'class'=> 'Manager', 'arguments'=> ['@@file_adapter', '++db'] ],
	['name'=> 'console_adapter', 'class'=> 'ConsoleAdapter', 'arguments'=> [] ],
];

echo '<pre>';
print_r($map);
echo '</pre>';


print_r('<br><b>1: How to use container</b><br>');

$parameters = array('db'=>  array('host'=> 'localhost', 'username'=> 'user', 'password'=> 'pass'));
$container = new Container($map, $parameters);

$manager = $container->get('manager');
if($manager) {
	$manager->write();
}

print_r('<br><b>1.2: Container use defined parameters</b><br>');
$manager = $container->get('manager');
echo ($manager->connect());

print_r('<br><b>2: Container modif</b>y<br>');
$container->set('manager', new Manager(new FileAdapter(null, 'new.csv')));
$manager = $container->get('manager');
if($manager) {
	$manager->write();
}

print_r('<br><b>3: Change behavior in container</b><br>');
$container->set('manager', new Manager(new ConsoleAdapter()));
$manager = $container->get('manager');
if($manager) {
	$manager->write();
}

print_r('<br><b>4: Parameters set and get</b><br>');
$container->setParameter('db', array('host'=> 'localhost', 'username'=> 'user', 'password'=> 'pass'));
$dbParams = $container->getParameter('db');
print_r($dbParams);

print_r('<br><b>5: Is it defined in container</b><br>');
echo ($container->has('file_adapter')) ? 'yes' : 'no';